
import * as React from 'react';
import './App.css';
import { getInitBee, bee } from './lib/bee-plugin';
import { pipe } from 'fp-ts/lib/pipeable';
import * as TE from 'fp-ts/lib/TaskEither';
import * as T from 'fp-ts/lib/Task';
import {f} from './lib/fetch';

interface Empty {
  type: 'Empty'
};

interface Loading {
  type: 'Loading'
};

interface Ready {
  type: 'Ready'
};

interface WithError {
  type: 'WithError',
  error: string
};

// --- State model
type AppState = Empty | Loading | Ready | WithError;

// --- Buttons models
interface WithError {
  type: 'WithError',
  error: string
};

type ButtonState = Ready | Loading | WithError;

// --- Component
export const App = () => {
  const [app, setApp] = React.useState<AppState>({type: 'Empty'});
  const [htmlBtn, setHtmlBtn] = React.useState<ButtonState>({type: 'Ready'});
  const [jsonBtn, setJsonBtn] = React.useState<ButtonState>({type: 'Ready'});

  React.useEffect(() => {
    const initBee = getInitBee(
      body =>
        pipe(
          f('/html', {method: 'POST', body: JSON.stringify(body)}),
          TE.fold(
            error => T.of(setHtmlBtn({
              type: 'WithError',
              error
            })),
            () => T.of(setHtmlBtn({
              type: 'Ready'
            }))
          )
        ),

      body =>
        pipe(
          f('/template', {method: 'PUT', body}),
          TE.fold(
            error => T.of(setJsonBtn({
              type: 'WithError',
              error
            })),
            () => T.of(setJsonBtn({
              type: 'Ready'
            }))
          )
        )
    );

    runApp(initBee, (error) => setApp({
      type: 'WithError',
      error
    }), () => setApp({
      type: 'Ready'
    }))();
  }, []);

  switch(app.type) {
    case 'WithError':
      return <main>Oops something went wrong!</main>;

    case 'Empty':
    case 'Loading':
    case 'Ready':
      return (
        <section className="app">
          {app.type !== 'Ready' ? <div className="loading ch_red"><i className="spinner fas fa-spinner"></i></div> : null }

          <header>
            <h1><i className="fas fa-tree"></i> <span className="ch_red">Christmas</span> Bee</h1>

            <button className="btn btn_preview" onClick={() => bee.preview()}>Preview</button>
          </header>

          <main id="bee-plugin-container"></main>

          <footer>
            <label>Save</label>
            <button className="btn btn_json" onClick={() => {
              setJsonBtn({type: 'Loading'});
              bee.saveAsTemplate();
            }}>
              {jsonBtn.type=== 'Loading' ? <i className="spinner fas fa-spinner"></i> : 'JSON'}
            </button>
            <button className="btn btn_hmtl" onClick={() => {
              setHtmlBtn({type: 'Loading'});
              bee.save();
            }}>
              {htmlBtn.type=== 'Loading' ? <i className="spinner fas fa-spinner"></i> : 'HTML'}
            </button>
          </footer>
        </section>
      );
  }
}

// --- Helpers
const runApp = (init: (template: Object) => TE.TaskEither<string, void>, he: (e: string) => void, hs: () => void) => pipe(
  f('/template'),
  TE.chain(res => TE.tryCatch(() => res.json(), String)),
  TE.chain(template => init(template)),
  TE.fold(
    e => T.of(he(e)),
    () => T.of(hs())
  )
);
