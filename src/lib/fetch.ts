import * as TE from 'fp-ts/lib/TaskEither';
import * as E from 'fp-ts/lib/Either';

const ENDPOINT = 'http://localhost:3001';
const BASE_HEADERS = {
  mode: 'cors',
  'Content-Type': 'application/json',
};

export const f = (route: string, reqOpts?: RequestInit): TE.TaskEither<string, Response> =>
  () =>
    fetch(
      `${ENDPOINT}${route}`,
      reqOpts? {
        ...reqOpts,
        headers: {
          ...BASE_HEADERS,
          ...reqOpts.headers
        }
      }: {
        headers: BASE_HEADERS
      }
    )
    .then(E.right)
    .catch(err => E.left(String(err)));
