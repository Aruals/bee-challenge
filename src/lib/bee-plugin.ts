// @ts-ignore
import Bee from '@mailupinc/bee-plugin';
import {CLIENT_ID, CLIENT_SECRET} from '../env';
import * as TE from 'fp-ts/lib/TaskEither';
import * as T from 'fp-ts/lib/Task';
import { pipe } from 'fp-ts/lib/pipeable';

export const bee = new Bee();

const getToken: TE.TaskEither<string, void> = TE.tryCatch(() => bee.getToken(CLIENT_ID, CLIENT_SECRET), String);

type SaveHtml = (body: {name: string, content: string}) => T.Task<void>;
type SaveJSON = (body: string) => T.Task<void>;

export const getInitBee = (sHTML: SaveHtml, sJSON: SaveJSON) => (template: Object): TE.TaskEither<string, void> => pipe(
  getToken,
  TE.chain(() =>
    TE.tryCatch(() => bee.start({
      uid: 'test1-laura',
      container: 'bee-plugin-container',
      language: 'en-EN',
      onSave: (_: string, htmlFile: string) =>
        sHTML({name: 'shinyHtml', content: htmlFile})(),
      onSaveAsTemplate: (jsonFile: string) =>
        sJSON(jsonFile)(),
      onSend: (htmlFile: string) => {
        console.log('onSend', htmlFile)
      },
      onError: (errorMessage: string) => {
        console.log('onError ', errorMessage)
      }
    }, template), String)
  )
);
