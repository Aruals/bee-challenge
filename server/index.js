const express = require("express");
const path = require("path");
const fs = require('fs');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();
app.use(bodyParser.json());
app.use(cors());

const port = process.env.PORT || "3001";

const TEMPLATES_FOLDER = path.join(__dirname, 'templates');
const HTMLS_FOLDER = path.join(__dirname, 'htmls');

// Get the saved template
app.get('/template', (req, res) => {
  fs.promises.readFile(path.join(TEMPLATES_FOLDER, 'base_m_bee.json'))
    .then(file => {
      res.type('application/json')
      res.status(200).send(file)
    })
    .catch(err => res.status(404).send(err));
});

// Update the template
app.put('/template', (req, res) => {
  fs.promises.writeFile(path.join(TEMPLATES_FOLDER, 'base_m_bee.json'), JSON.stringify(req.body))
    .then(() => res.status(200).send(req.body))
    .catch(err => res.status(404).send(err));
});

// Save the html
app.post('/html', (req, res) => {
  const {
    name,
    content
  } = req.body;

  if (!fs.existsSync(HTMLS_FOLDER)) {
    fs.mkdirSync(HTMLS_FOLDER);
  }

  fs.promises.writeFile(path.join(HTMLS_FOLDER, `${name}_nl_${Date.now()}.html`), content)
    .then(() => res.status(200).send(req.body))
    .catch(err => res.status(404).send(err));
});

app.listen(port, () => {
  console.log(`Listening to requests on http://localhost:${port}`);
});
