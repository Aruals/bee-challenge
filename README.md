This project is made for the BEE Challenge on Open Data Playground

## How to start the SPA

- Install the dependencies.

  ```
  $ npm ci
  ```

- Create a file "env.ts" in the /src folder, it must export your clientId and clientSecret variables, like this:

  ```js
  export const CLIENT_ID = 'your-client-id';
  export const CLIENT_SECRET = 'your-client-secret';
  ```

- Run the command:

  ```
    $ npm run start
  ```
- Open http://localhost:3000

## Where are saved the files

- The htmls in [./server/htmls](./server/htmls)
- The json template in [./server/templates](./server/templates)

