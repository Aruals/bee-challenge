const path = require('path');
const merge = require('webpack-merge');
const commons = require('./webpack.common');

module.exports = merge(commons, {
  mode: 'development',

  optimization: {
    removeAvailableModules: false
  },

  devtool: 'eval-source-map',

  devServer: {
    contentBase: path.join(process.cwd(), 'src'),
    publicPath: '/',
    compress: true,
    host: '0.0.0.0',
    disableHostCheck: true,
    port: 3000,
    watchOptions: {
      ignored: ['/node_modules'],
      poll: false
    },
    hot: false,
    inline: false
  }
});
