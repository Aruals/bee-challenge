const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const webpack = require('webpack');

const ROOT = process.cwd();
const pkg = require(path.join(ROOT, 'package.json'));

const SRC = path.join(ROOT, 'src');
const DIST = path.join(ROOT, 'dist')

const commons = {
  bail: true,

  context: ROOT,

  entry: {
    app: SRC
  },

  output: {
    path: DIST,
    pathinfo: false,
    filename: '[name].[contenthash].bundle.js'
  },

  module: {
    rules: [{
        test: /\.css$/,
        loader: 'style-loader'
      },{
        test: /\.css$/,
        loader: 'css-loader'
      },
      {
        test: /\.ts(x?)$/,
        exclude: /node_modules/,
        use: [{
            loader: 'cache-loader',
            options: {
              cacheDirectory: path.join(ROOT, '.cache', 'cache-loader')
            }
          },
          {
            loader: 'babel-loader',
            options: {
              babelrc: true
            }
          },
          {
            loader: 'ts-loader',
            options: {
              transpileOnly: true,
              configFile: path.join(ROOT, 'tsconfig.json')
            }
          }
        ]
      }
    ]
  },

  resolve: {
    extensions: ['.ts', '.tsx', '.js']
  },

  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        APP_VERSION: JSON.stringify(pkg.version)
      }
    }),

    new ForkTsCheckerWebpackPlugin({
      checkSyntacticErrors: true,
      watch: [SRC]
    }),

    new HtmlWebpackPlugin({
      template: 'src/index.ejs',
      version: pkg.version,
      minify: {
        removeComments: true,
        collapseWhitespace: true
      },
      inject: false
    })
  ]
};

module.exports = commons;
