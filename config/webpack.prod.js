const path = require('path');
const merge = require('webpack-merge');
const commons = require('./webpack.common');
const TerserPlugin = require('terser-webpack-plugin');

module.exports = merge(commons, {
  mode: 'production',

  output: {
    path: path.join(process.cwd(), 'dist')
  },

  devtool: 'hidden-source-map',

  optimization: {
    minimizer: [
      new TerserPlugin({
        terserOptions: {
          ecma: 6
        },
        parallel: true,
        sourceMap: true
      })
    ]
  }
});
